import { Component, OnInit } from '@angular/core';
import{ FormGroup,FormControl} from '@angular/forms'
import {Validators} from '@angular/forms'
import {Router,Route} from '@angular/router'
import { FormdataService } from '../formdata.service';

@Component({
  selector: 'app-formpage',
  templateUrl: './formpage.component.html',
  styleUrls: ['./formpage.component.css']
})
export class FormpageComponent implements OnInit {
  title = 'reactivepage';
  check:boolean=false
  logForm: FormGroup;
  change:string="password";
  
  lstr(){
     this.userinputdata.check(this.logForm.value);
     this.route.navigate(['/datapage'])
  //   localStorage.setItem("FormData",JSON.stringify(this.logForm.value))
  //   this.tableview();
  }
  showHide(){
    if(this.change==='password'){
      this.change='text'
    }else{
      this.change='password'
    }
  }
  
  passWordCall(){
    if(this.logForm.value.pass1 !== this.logForm.value.pass2){
      this.check= false
    }else{
      this.check= true;
      
    }
    // console.log(this.logForm.value.pass1)
  }
 

  constructor(private route:Router,private userinputdata:FormdataService) {
    this.logForm = new FormGroup({
      firstName: new FormControl('', [Validators.required, Validators.pattern("^[a-zA-Z]+$"),]),
      lastName: new FormControl('', [Validators.required, Validators.pattern("^[a-zA-Z]+$"),]),
      emails: new FormControl('', [Validators.required,]),
      cno: new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern("^(([0-9]*)|(([0-9]*)\.([0-9]*)))$")]),
       pass1: new FormControl('', [Validators.required, Validators.minLength(8), /*Validators.pattern("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})")*/]),
       pass2: new FormControl('', [Validators.required, Validators.minLength(8),/*Validators.pattern("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})")*/]),
      eid: new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(4), Validators.pattern("^(([0-9]*)|(([0-9]*)\.([0-9]*)))$")]),
      gender: new FormControl('', [Validators.required,Validators.minLength(1),Validators.maxLength(1), Validators.pattern("^[a-zA-Z]+$"),]),



    }
    )
  }
  

  ngOnInit() {
    
    if(this.route.url==='/formpage'){
   /*   let edit=JSON.parse(localStorage.getItem("FormData"));*/
   var edit=this.userinputdata.getdata();
      this.logForm.patchValue({
        firstName: edit.firstName,
        lastName : edit.lastName,
        cno: edit.cno,
        gender: edit.gender,
        eid: edit.eid,
        emails: edit.emails,
        pass1:edit.pass1,
        pass2:edit.pass2
      })
    }
    

}


}
