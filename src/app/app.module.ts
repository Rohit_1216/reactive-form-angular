import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import {FormdataService} from './formdata.service';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule} from '@angular/forms';
import { FormControl } from '@angular/forms';
import { DatapageComponent } from './datapage/datapage.component';
import { RouterModule,Routes } from '@angular/router';

import { FormpageComponent } from './formpage/formpage.component';

const route:Routes=[
  {
    path: '',
    component: FormpageComponent
  },
  { path: 'datapage',
  component: DatapageComponent

  },
  {
    path: 'formpage',
    component: FormpageComponent
  }
  
  
];
@NgModule({
  declarations: [
    AppComponent,
    DatapageComponent,
   
    FormpageComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forRoot(route)
  ],
  providers: [FormdataService ],
  bootstrap: [AppComponent],
  
})
export class AppModule { 
  name = new FormControl('');
}
