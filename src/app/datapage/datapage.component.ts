import { Component, OnInit } from '@angular/core';
import { Routes,Router } from '@angular/router';
import {FormdataService} from '../formdata.service'

@Component({
  selector: 'app-datapage',
  templateUrl: './datapage.component.html',
  styleUrls: ['./datapage.component.css']
})
export class DatapageComponent implements OnInit {

  constructor(private route:Router,private servar:FormdataService) { 
    this.dataValue=this.servar.getdata();
  }
  
  editpage(){
    this.route.navigate(['/formpage']);
    

  }

  dataValue:any;
  ngOnInit() {
  
  //this.dataValue = JSON.parse(localStorage.getItem('FormData'))
}
}
